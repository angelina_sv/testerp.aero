import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    type: 'varchar',
    unique: true,
    nullable: true,
  })
  email!: string | null;

  @Column({
    type: 'varchar',
    unique: true,
    nullable: true,
  })
  phone!: string | null;

  @Column({
    type: 'varchar',
  })
  password!: string;

  @Column({
    type: 'varchar',
    nullable: true,
    unique: true,
  })
  refreshToken!: string | null;
}
