import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class File {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    type: 'varchar',
  })
  name!: string;

  @Column({
    type: 'varchar',
  })
  extension!: string;

  @Column({
    type: 'varchar',
  })
  mimeType!: string;

  @Column({
    type: 'int',
  })
  size!: number;

  @Column({
    type: 'datetime',
  })
  downloadDate!: Date;

  @Column({
    type: 'varchar',
    unique: true,
  })
  path!: string;
}
