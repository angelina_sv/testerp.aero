import * as dotenv from 'dotenv';
import { boostrap } from './app';

const port = process.env.PORT || '3000';

boostrap().then((app) => {
  dotenv.config();
  app.listen(port, () => {
    console.log(`App listening at htt://localhost:${port}`);
  });
});
