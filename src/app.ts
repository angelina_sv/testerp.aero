import * as express from 'express';
import * as cors from 'cors';
import * as multer from 'multer';

import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { UserController } from './user/user.controller';
import { FileController } from './file/file.controller';
import { FileService } from './file/file.service';
import { ensureAuthorized } from './auth/auth.middleware';

async function boostrap() {
  const authService = new AuthService();
  const fileService = new FileService();

  await Promise.all([authService.init(), fileService.init()]);

  const authController = new AuthController(authService);
  const fileController = new FileController(fileService);
  const userController = new UserController();

  const app = express();

  app.use(cors());
  app.use(express.json({ limit: '50mb' }));
  app.use(express.urlencoded({ extended: false, limit: '50mb' }));

  const storage = multer.memoryStorage();
  const fileIntercepter = multer({ storage });

  app.post('/signin', authController.signIn.bind(authController));
  app.post('/signin/new_token', authController.updateToken.bind(authController));
  app.post('/signup', authController.signUp.bind(authController));
  app.get('/logout', ensureAuthorized, authController.logout.bind(authController));

  app.get('/info', ensureAuthorized, userController.getInfo.bind(userController));

  app.post('/file/upload', ensureAuthorized, fileIntercepter.single('document'), fileController.upload.bind(fileController));
  app.get('/file/list', ensureAuthorized, fileController.getList.bind(fileController));
  app.delete('/file/delete/:id', ensureAuthorized, fileController.delete.bind(fileController));
  app.get('/file/:id', ensureAuthorized, fileController.getInfo.bind(fileController));
  app.get('/file/download/:id', ensureAuthorized, fileController.download.bind(fileController));
  app.put('/file/update/:id', ensureAuthorized, fileIntercepter.single('document'), fileController.update.bind(fileController));

  app.use((err: Error, _req: any, res: express.Response, _next: express.NextFunction) => {
    console.error(err.stack);
    res.status((err as any).status || 500).send(err.message);
  });

  return app;
}

export { boostrap };
