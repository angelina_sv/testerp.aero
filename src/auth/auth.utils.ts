class AccessTokenCache {
  private cacheSet = new Map<string, Date>();

  constructor() {
    setInterval(() => this.handleTokens(), 5 * 60 * 1000);
  }

  add(token: string, expirationDate: number) {
    this.cacheSet.set(token, new Date(expirationDate));
  }

  has(token: string) {
    return this.cacheSet.has(token);
  }

  remove(token: string) {
    return this.cacheSet.delete(token);
  }

  handleTokens() {
    this.cacheSet.forEach((value, key) => {
      if (Date.now() > value.getTime()) {
        this.cacheSet.delete(key);
      }
    });
  }
}

export const accessTokenCache = new AccessTokenCache();
