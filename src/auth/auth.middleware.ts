import { Request, Response, NextFunction } from 'express';
import * as createError from 'http-errors';
import { verifyToken } from './auth.service';
import { accessTokenCache } from './auth.utils';

export async function ensureAuthorized(req: Request, _res: Response, next: NextFunction) {
  try {
    const token = req.headers['bearer-token'] as string;

    if (!accessTokenCache.has(token)) {
      return next(createError(401, 'Unauthorized'));
    }

    const decoded = await verifyToken(token) as any;
    (req as any).userId = decoded.data.id;
    (req as any).token = token;

    return next();
  } catch (err) {
    return next(err);
  }
}
