import { Request, Response, NextFunction } from 'express';

import { AuthService } from './auth.service';

export class AuthController {
  constructor(private authService: AuthService) {}

  async signIn(req: Request, res: Response, next: NextFunction) {
    try {
      const { id, password } = req.body;
      const result = await this.authService.signIn(id, password);
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  }

  async updateToken(req: Request, res: Response, next: NextFunction) {
    try {
      const { refreshToken } = req.body;
      const accessToken = await this.authService.updateToken(refreshToken);
      res.status(200).send(accessToken);
    } catch (err) {
      next(err);
    }
  }

  async signUp(req: Request, res: Response, next: NextFunction) {
    try {
      const { id, password } = req.body;
      const result = await this.authService.signUp(id, password);
      res.status(201).send(result);
    } catch (err) {
      next(err);
    }
  }

  async logout(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId, token } = req as any;
      await this.authService.logout(userId, token);
      res.status(204).send();
    } catch (err) {
      next(err);
    }
  }
}
