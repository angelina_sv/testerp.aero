import * as createError from 'http-errors';
import validator from 'validator';
import * as jwt from 'jsonwebtoken';
import * as crypto from 'crypto';
import * as bcrypt from 'bcrypt';

import { Repository } from 'typeorm/repository/Repository';
import { wrapper } from '../db';
import { User } from '../entity/User';
import { accessTokenCache } from './auth.utils';

export class AuthService {
  private userRepository!: Repository<User>;

  private static readonly ACCESS_TOKEN_EXPIRATION = 600;

  async init() {
    this.userRepository = await wrapper.getRepository(User);
  }

  async signIn(id: string, password: string) {
    const { email, phone } = AuthService.determineIdType(id);

    const user = await this.userRepository.findOne({
      where: {
        email,
        phone,
      },
    });

    if (!user) {
      throw createError(404, 'User with such data was not found');
    }

    if (!(await bcrypt.compare(password, user.password as string))) {
      throw createError(401, 'Unauthorized');
    }

    const { expirationDate, encodedToken } = await AuthService.generateAccessToken(user.id);
    const refreshToken = await AuthService.generateRefreshToken();

    accessTokenCache.add(encodedToken!, expirationDate);
    user.refreshToken = refreshToken as string;
    await this.userRepository.save(user);

    return { accessToken: encodedToken, refreshToken };
  }

  async updateToken(refreshToken: string) {
    const userByToken = await this.checkRefreshToken(refreshToken);
    const { expirationDate, encodedToken } = await AuthService.generateAccessToken(userByToken.id);
    accessTokenCache.add(encodedToken as string, expirationDate);

    return { accessToken: encodedToken };
  }

  async signUp(id: string, password: string) {
    const { email, phone } = AuthService.determineIdType(id);
    const existingUser = await this.userRepository.findOne({
      where: {
        email,
        phone,
      },
    });

    if (existingUser) {
      throw createError(403, 'User with this email or phone already exists');
    }
    const refreshToken = await AuthService.generateRefreshToken();

    const newUser = new User();
    newUser.email = email;
    newUser.phone = phone;
    newUser.password = await bcrypt.hash(password, 10);
    newUser.refreshToken = refreshToken as string;
    const savedUser = await this.userRepository.save(newUser);

    const { expirationDate, encodedToken } = await AuthService.generateAccessToken(savedUser.id);
    accessTokenCache.add(encodedToken as string, expirationDate);

    return { accessToken: encodedToken, refreshToken };
  }

  async logout(userId: number, token: string) {
    const targetUser = await this.userRepository.findOne(userId);
    if (!targetUser) {
      throw createError(404, 'User not found');
    }

    targetUser.refreshToken = null;
    await this.userRepository.save(targetUser);

    accessTokenCache.remove(token);
  }

  private async checkRefreshToken(refreshToken: string) {
    const result = await this.userRepository.findOne({
      where: { refreshToken },
    });
    if (!result) {
      throw createError(401, 'Bad refresh token');
    } else {
      return result;
    }
  }

  private static determineIdType(id: string) {
    if (validator.isEmail(id)) {
      return {
        email: id,
        phone: null,
      };
    } if (validator.isMobilePhone(id)) {
      return {
        email: null,
        phone: id,
      };
    }
    throw createError(400, 'Incorrect email or phone number');
  }

  private static async generateAccessToken(userId: number): Promise<{
    expirationDate: number,
    encodedToken: string | undefined,
  }> {
    const expirationDate = Math.floor((Date.now() / 1000)) + this.ACCESS_TOKEN_EXPIRATION;
    return new Promise((res, rej) => {
      jwt.sign({
        exp: expirationDate,
        data: { id: userId },
      },
      process.env.TOKEN_PRIVATE_KEY!,
      (err: Error | null, encodedToken: string | undefined) => {
        if (err) {
          return rej(createError(500, `Error througth generating access token reason: ${err.message}`));
        }
        return res({
          expirationDate,
          encodedToken,
        });
      });
    });
  }

  private static async generateRefreshToken() {
    return new Promise((res, rej) => {
      crypto.randomBytes(64, (err, buff) => {
        if (err) {
          return rej(createError(500, `Error througth generating refresh token reason: ${err.message}`));
        }
        return res(buff.toString('hex'));
      });
    });
  }
}

export async function verifyToken(token: string) {
  return new Promise((res, rej) => {
    jwt.verify(
      token,
      process.env.TOKEN_PRIVATE_KEY!,
      (err, decoded) => {
        if (err) {
          return rej(createError(401, 'Invalid token'));
        }
        return res(decoded);
      },
    );
  });
}
