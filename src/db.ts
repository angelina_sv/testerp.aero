import { createConnection, Connection } from 'typeorm';

interface Type<T = any> extends Function {
  new (...args: any[]): T;
}

class TypeormWrapper {
  private connection!: Connection;

  async getRepository<TEntity>(target: Type<TEntity>) {
    if (!this.connection) {
      this.connection = await createConnection();
    }

    return this.connection.getRepository(target);
  }
}

export const wrapper = new TypeormWrapper();
