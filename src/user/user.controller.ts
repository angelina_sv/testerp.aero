import { Request, Response, NextFunction } from 'express';

export class UserController {
  // eslint-disable-next-line class-methods-use-this
  async getInfo(req: Request, res: Response, next: NextFunction) {
    try {
      res.status(200).send({ id: (req as any).userId });
    } catch (err) {
      next(err);
    }
  }
}
