import * as createError from 'http-errors';
import * as fs from 'fs';
import * as path from 'path';
import { v4 as uuidv4 } from 'uuid';

import { Repository } from 'typeorm/repository/Repository';
import { File } from '../entity/File';
import { wrapper } from '../db';

export class FileService {
  private fileRepository!: Repository<File>;

  private readonly FILES_FOLDER = 'files';

  private readonly savedFilesPath = path.join(process.cwd(), this.FILES_FOLDER);

  async init() {
    this.fileRepository = await wrapper.getRepository(File);
  }

  async upload(rawFile: Express.Multer.File) {
    await this.checkOrCreateDir(this.savedFilesPath);
    const fileName = uuidv4();
    await fs.promises.writeFile(path.join(this.savedFilesPath, fileName), rawFile.buffer);

    const file = new File();
    file.name = rawFile.originalname;
    // eslint-disable-next-line prefer-destructuring
    file.extension = rawFile.originalname.split('.').reverse()[0];
    file.mimeType = rawFile.mimetype;
    file.size = rawFile.size;
    file.downloadDate = new Date();
    file.path = path.join(this.FILES_FOLDER, fileName);
    await this.fileRepository.save(file);
  }

  async getList(page: number = 1, listSize: number = 10) {
    const offset = page === 1 ? 0 : ((page - 1) * listSize);
    const limit = listSize;

    const list = await this.fileRepository.find({
      skip: offset,
      take: limit,
    });

    return list;
  }

  async delete(id: number) {
    const file = await this.fileRepository.findOne(id);

    if (!file) {
      throw createError(404, `File with id ${id} not found`);
    }

    await fs.promises.unlink(path.join(process.cwd(), file.path));
    await this.fileRepository.remove(file);
  }

  async getInfo(id: number) {
    const file = await this.fileRepository.findOne(id);

    if (!file) {
      throw createError(404, `File with id ${id} not found`);
    }

    return file;
  }

  async update(id: number, rawFile: Express.Multer.File) {
    const file = await this.fileRepository.findOne(id);

    if (!file) {
      throw createError(404, `File with id ${id} not found`);
    }

    await fs.promises.unlink(path.join(process.cwd(), file.path));
    const fileName = uuidv4();
    await fs.promises.writeFile(path.join(this.savedFilesPath, fileName), rawFile.buffer);

    file.name = rawFile.originalname;
    // eslint-disable-next-line prefer-destructuring
    file.extension = rawFile.originalname.split('.').reverse()[0];
    file.mimeType = rawFile.mimetype;
    file.size = rawFile.size;
    file.path = path.join(this.FILES_FOLDER, fileName);
    file.downloadDate = new Date();

    await this.fileRepository.save(file);
  }

  // eslint-disable-next-line class-methods-use-this
  async checkOrCreateDir(dirPath: string) {
    try {
      await fs.promises.stat(dirPath);
    } catch (err) {
      if (err.code === 'ENOENT') {
        await fs.promises.mkdir(dirPath, { recursive: true });
      }
      throw createError(500, `Internal server error reason: ${err.message}`);
    }
  }
}
