import { Request, Response, NextFunction } from 'express';
import * as createError from 'http-errors';
import * as path from 'path';
import validator from 'validator';

import { FileService } from './file.service';

export class FileController {
  private fileService: FileService;

  constructor(fileService: FileService) {
    this.fileService = fileService;
  }

  async upload(req: Request, res: Response, next: NextFunction) {
    try {
      const { file } = req as any;
      await this.fileService.upload(file);
      res.status(201).send();
    } catch (err) {
      next(err);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async getList(req: Request, res: Response, next: NextFunction) {
    try {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      const { page, list_size: listSize } = req.query as any;
      if (page && !validator.isInt(page)) {
        throw createError('page must be an integer');
      }
      if (listSize && !validator.isInt(listSize)) {
        throw createError('list_size must be an integer');
      }

      const result = await this.fileService.getList(Number(page), Number(listSize));
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  }

  async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      if (!validator.isInt(id as string)) {
        throw createError('id must be an integer');
      }

      await this.fileService.delete(Number(id));
      res.status(204).send();
    } catch (err) {
      next(err);
    }
  }

  async getInfo(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      if (!validator.isInt(id as string)) {
        throw createError('id must be an integer');
      }

      const result = await this.fileService.getInfo(Number(id));
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  }

  async download(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      if (!validator.isInt(id as string)) {
        throw createError('id must be an integer');
      }

      const file = await this.fileService.getInfo(Number(id));
      res.status(200).download(path.join(process.cwd(), file.path), file.name);
    } catch (err) {
      next(err);
    }
  }

  async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { file } = req as any;
      const { id } = req.params;

      if (!validator.isInt(id as string)) {
        throw createError('id must be an integer');
      }

      await this.fileService.update(Number(id), file);
      res.status(200).send();
    } catch (err) {
      next(err);
    }
  }
}
